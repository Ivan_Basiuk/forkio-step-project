# Спільний степ проект Forkio

## Учасники:
- Іван Басюк
- Віталій Пасечник

Студент Іван виконував завдання:
- Зверстати шапку сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана.
- Зверстати секцію People Are Talking About Fork.

Студент Віталій виконував завдання:
- Зверстати блок Revolutionary Editor. Кнопки треба зробити, щоб виглядали як тут справа вгорі (звідти ж за допомогою інспектора можна взяти всі SVG іконки і завантажити стилі, що використовуються на гітхабі).
- Зверстати секцію Here is what you get.
- Зверстати секцію Fork Subscription Pricing. У блоці з цінами третій елемент завжди буде "виділений" і буде більшим за інші (тобто не по кліку/ховеру, а статично).

Під час виконання проекту використовували технології:
- Html
- Css
- Js
- Scss
- Gulp
- SVG sprite
